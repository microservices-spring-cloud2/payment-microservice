package com.classpath.payment.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.classpath.payment.model.Payment;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/v1/payment")
@Slf4j
public class PaymentController {
	
	@PostMapping
	public ResponseEntity<Payment> acceptPayment(@RequestBody Payment payment){
		log.info("Accepted the payment " +payment.getMoney() + " from "+ payment.getFrom());
		payment.setStatus("SUCCESS");
		return new ResponseEntity<Payment>(payment, HttpStatus.OK);
		
	}

}
